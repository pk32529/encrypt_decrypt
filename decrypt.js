const crypto = require('crypto');
const forge = require('node-forge');
const fs = require('fs');

const oldPrivate = fs.readFileSync('./old/server.crt');
const oldPublic = fs.readFileSync('./old/server_key.pem');
const newPublic = fs.readFileSync('./new/DSB_Public_certificate.crt');

const CBSMESSAGE = 'di6gG5UcC7osVFolHbeNIUzHA1/SOKuvi/wZi4msjJN8wq/jDjpQ7fGDnS9it3jfNVf6M1tmsz6e0g9DboxXLh7a7W0qQHR19w1t29PaxLkl9dNACMRKJC3TxA73VqJWo6Q+0pAJgDb5JEfOyyLD/551w6xi7TTVaWrpuzpfjejIsBdYOnxkrWeh8tIWQJwAIuP9QL4NUmaV46g6xmIBxZhjjcRC6bFSH62SfkNDKZvddyQcE4vvhS4UQakZLdzM9+C2mUBE5AUOXNvEMEHHz3THiSHXmvvU1IavascSaju46zFmbywIFL6l/MX8L2zNRDcLm0wNqq/MauimhwTeIA==';

var keypair = {};
let pvtKey = oldPublic;
keypair.privateKey = forge.pki.decryptRsaPrivateKey(pvtKey);

var decrypted = keypair.privateKey.decrypt(forge.util.decode64(CBSMESSAGE), 'RSA-OAEP', {
    md: forge.md.sha256.create(),
    mgf1: {
        md: forge.md.sha1.create()
    }
});

console.log(decrypted);