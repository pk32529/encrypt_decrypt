const crypto = require('crypto');
const forge = require('node-forge');
const fs = require('fs');

const oldPublic = fs.readFileSync('./old/server.crt');
const oldPrivate = fs.readFileSync('./old/server_key.pem');
const newPublic = fs.readFileSync('./new/DSB_Public_certificate.crt');

const CBSMESSAGE = 'hi what are you doing 123';

var keypair = {};
let pubKey = newPublic;
var cert = forge.pki.certificateFromPem(pubKey);
keypair.publicKey = cert.publicKey;

var encrypted = forge.util.encode64(keypair.publicKey.encrypt(CBSMESSAGE, 'RSA-OAEP', {
    md: forge.md.sha256.create(),
    mgf1: {
        md: forge.md.sha1.create()
    }
}));

console.log(encrypted);